import org.apache.hadoop.yarn.webapp.hamlet.HamletSpec.Listing
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
/**
  * Created by mac029 on 2017/5/1.
  */
object HelloSpark extends App {

  val conf = new SparkConf().setAppName("Hello")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)
  //readLine()//run時不會停
  val nums: RDD[String] = sc.textFile("nums.txt")
  //textfile 斷行
   //nums.foreach(str=>{       //foreach 對每一行
  //println("====")
  //println(str)
  //println(nums.count())   // count 算行
  // val intrdd:RDD[Int]=sc.parallelize(1 to 100)
  //println(intrdd.sum())
  val range = 1 to 100 //range~=list
  val intrdd1: RDD[Int] = sc.parallelize(range)
  println("range:" + range)
  println("rdd:" + intrdd1)


}
