import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac029 on 2017/5/1.
  */
object sq extends App{
  val conf = new SparkConf().setAppName("square")
    .setMaster("local[*]")

  val sc = new SparkContext(conf)

//  val intrdd=sc.parallelize(1 to 100000)
//  val square=intrdd.map(x=>x*x)
//  square.take(10).foreach(println)

  //square.collect().foreach(println)
  //first
  sc.textFile("./name.txt").filter(x=>x.head=='r').foreach(println)

  val kv=sc.parallelize(1 to 100).map(v=>{
    if (v%2==0) "even"->v else "odd"->v
  })
  //kv.mapValues(_+1).take(10).foreach(println)
  kv.groupByKey().mapValues(v=>v.sum).foreach(println)
  kv.reduceByKey((ac,cur)=>ac+cur).foreach(println) // groupbykey,reduce相同結果 reduce 實務上較好

}


